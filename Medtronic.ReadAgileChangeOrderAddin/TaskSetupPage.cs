﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Medtronic.ReadAgileChangeOrderAddin
{
    public partial class TaskSetupPage : UserControl
    {
        public TaskSetupPage()
        {
            InitializeComponent();
        }

        public TaskSettings Settings
        {
            get {
                return (TaskSettings)this.propertyGrid1.SelectedObject;
            }
            set {
                this.propertyGrid1.SelectedObject = value;
            }
        }
    }
}
