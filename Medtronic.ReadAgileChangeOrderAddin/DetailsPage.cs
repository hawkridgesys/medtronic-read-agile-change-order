﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Medtronic.ReadAgileChangeOrderAddin
{
    [ComVisible(true)]
    [Guid("6BB66FDF-970D-4327-81D8-44444EE76B7F")]
    public partial class DetailsPage : UserControl
    {
        public DetailsPage()
        {
            InitializeComponent();
        }

        public void LoadData(string logText)
        {
            this.textBox1.Lines = logText.Split('\n');
        }
    }
}
