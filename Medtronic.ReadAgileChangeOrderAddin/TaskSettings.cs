﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medtronic.ReadAgileChangeOrderAddin
{
    public class TaskSettings
    {
        [Category("Settings"), Description("Folder path where the XML change orders are stored."), DisplayName("Agile XML Folder")]
        public string AgileXmlFolder { get; set; }

        [Category("Settings"), Description("Folder path to move the processed change orders to when complete."), DisplayName("Completed Folder")]
        public string CompleteAgileXmlFolder { get; set; }

        [Category("Settings"), Description("Specify the PDM state to move the change order for each type of Agile change order status."), DisplayName("Agile Status Mappings")]
        public List<AgileStatus> AgileStatuses { get; set; } = new List<AgileStatus>();
    }

    public class AgileStatus
    {
        [Category("Settings"), Description("Agile change order status.")]
        public string Status{ get; set; }

        [Category("Settings"), Description("Name of transition.")]
        public string StateName { get; set; }
        public override string ToString()
        {
            return $"{Status} - {StateName}";
        }
    }
}
