﻿using EdmLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medtronic.ReadAgileChangeOrderAddin
{
    public static class Extensions
    {
        public static void Log(this IEdmTaskInstance instance, string text)
        {
            string name = instance.InstanceGUID + "-Log";
            if (instance.GetValEx(name) != null) {
                instance.SetValEx(name, instance.GetValEx(name).ToString() + '\n' + text);
            }
            else {
                instance.SetValEx(name, text);
            }
        }

        public static string GetLog(this IEdmTaskInstance instance)
        {
            string name = instance.InstanceGUID + "-Log";
            return (string)instance.GetValEx(name);
        }
    }
}
