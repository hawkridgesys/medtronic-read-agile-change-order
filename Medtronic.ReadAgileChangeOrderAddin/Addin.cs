﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using EdmLib;

namespace Medtronic.ReadAgileChangeOrderAddin
{
    [ComVisible(true)]
    [Guid("3477A38C-6521-41F9-B9CC-8020E13B917B")]
    public class Addin : IEdmAddIn5
    {

        private const string NAME = "Medtronic Read Agile Change Order";
        private const string COMPANY = "Hawk Ridge Systems";
        private const string DESCRIPTION = "Task addin to read Agile change order.";
        private const int VERSION = 1;
        private const int MAJOR = 16;
        private const int MINOR = 1;

        private TaskSettings _taskSettings;

        public void GetAddInInfo(ref EdmAddInInfo poInfo, IEdmVault5 poVault, IEdmCmdMgr5 poCmdMgr)
        {
            poInfo.mbsAddInName = NAME;
            poInfo.mbsCompany = COMPANY;
            poInfo.mbsDescription = DESCRIPTION;
            poInfo.mlAddInVersion = VERSION;
            poInfo.mlRequiredVersionMajor = MAJOR;
            poInfo.mlRequiredVersionMinor = MINOR;

            // add hooks
            poCmdMgr.AddHook(EdmCmdType.EdmCmd_TaskRun);
            poCmdMgr.AddHook(EdmCmdType.EdmCmd_TaskSetup);
            poCmdMgr.AddHook(EdmCmdType.EdmCmd_TaskSetupButton);
            poCmdMgr.AddHook(EdmCmdType.EdmCmd_TaskDetails);

        }

        /// Called when command is invoked.
        /// </summary>
        /// <param name="poCmd"></param>
        /// <param name="ppoData"></param>
        public void OnCmd(ref EdmCmd poCmd, ref EdmCmdData[] ppoData)
        {
#if DEBUG
            AttachDebugger();
#endif

            IEdmVault11 vault = (IEdmVault11)poCmd.mpoVault;


            switch (poCmd.meCmdType) {
                case EdmCmdType.EdmCmd_TaskLaunch:
                    // we're not supporting on task launch
                    OnTaskLaunch(poCmd, ppoData);
                    break;
                case EdmCmdType.EdmCmd_TaskRun:
                    // called when the task begins to run
                    OnTaskRun(poCmd, ppoData);
                    break;
                case EdmCmdType.EdmCmd_TaskSetup:
                    // called when we set the task up
                    OnTaskSetup(poCmd, ppoData);
                    break;
                case EdmCmdType.EdmCmd_TaskSetupButton:
                    // called when the user clicks ok on the task setup
                    OnTaskSetupButton(poCmd, ppoData);
                    break;
                case EdmCmdType.EdmCmd_TaskDetails:
                    // called when the user clicks the details on the task setup
                    OnTaskDetails(ref poCmd, ppoData);
                    break;
                default:
                    break;
            }


        }


        #region Task Callbacks

        /// <summary>
        /// You need to specify the EdmTask_SupportsInitExec in the IEdmTaskProperties::TaskFlags property in order to get this call. 
        /// The launch call makes it possible for you to display a user interface where the user selects files or enters data in a custom dialog box.
        /// 
        /// EdmCmdData  Type    Description
        /// mlObjectID1 integer ID of the selected object (IEdmObject5::ID)
        /// mlObjectID2 integer Parent folder ID if the selected object is a file
        /// mbsStrData1 string  Complete file system path to the object
        /// mbsStrData2 string  Configuration name if the object is a file
        /// mlLongData1 integer EdmObjectType constant telling what kind of object this is
        /// </summary>
        /// <param name="poCmd"></param>
        /// <param name="ppoData"></param>
        private void OnTaskLaunch(EdmCmd poCmd, EdmCmdData[] ppoData)
        {
            // we are not supporting launching tasks at this time
        }

        /// <summary>
        /// This call is made to your add-in when the task details dialog box is displayed from the task list in the administration tool. 
        /// You must specify the flag EdmTaskFlag.EdmTask_SupportsDetails in the IEdmTaskProperties::TaskFlags property in order to get the EdmCmd_TaskDetails call. 
        /// You can set the flag when you get the EdmCmdType.EdmCmd_TaskSetup call. EdmCmd::mpoExtra points to IEdmTaskInstance, the interface of the task instance.
        /// 
        /// The EdmCmd::mpoExtra pointer should be set to your implementation of the extra page.
        /// The framework keeps this pointer referenced until the dialog box is closed.
        /// You return the window handle of the extra page in the member EdmCmd::mlParentWnd.
        /// The property dialog box calls ShowWindow when the page is displayed or hidden.
        /// 
        /// None of the members in the EdmCmdData structure are used.
        /// 
        /// </summary>
        /// <param name="poCmd"></param>
        /// <param name="ppoData"></param>
        private void OnTaskDetails(ref EdmCmd poCmd, EdmCmdData[] ppoData)
        {
            IEdmTaskInstance instance = (IEdmTaskInstance)poCmd.mpoExtra;

            var details = new DetailsPage();
            details.CreateControl();
            poCmd.mpoExtra = details;
            poCmd.mbsComment = "Log";
            poCmd.mlParentWnd = details.Handle.ToInt32();
            details.LoadData(instance.GetLog());
        }

        /// <summary>
        /// This is the callback that is called when the actual work of the task is supposed to be executed. 
        /// The task is usually executed on a remote server so you must not display any user interface during this call. 
        /// 
        /// See the OnTaskRun function in the task sample code for an example of how to return status information and errors to the user.
        /// 
        /// EdmCmd::mpoExtra points to the IEdmTaskInstance interface of the task instance.
        /// 
        /// EdmCmdData  Type    Description
        /// mlObjectID1 integer ID of the selected object (IEdmObject5::ID)
        /// mlObjectID2 integer Parent folder ID if the selected object is a file
        /// mbsStrData1 string  Complete file system path to the object
        /// mbsStrData2 string  Configuration name if the object is a file
        /// mlLongData1 integer EdmObjectType constant telling what kind of object this is
        /// </summary>
        /// <param name="poCmd"></param>
        /// <param name="ppoData"></param>
        private void OnTaskRun(EdmCmd poCmd, EdmCmdData[] ppoData)
        {
            // get the vault
            IEdmVault10 vault = (IEdmVault10)poCmd.mpoVault;
            // get the task intance
            IEdmTaskInstance instance = poCmd.mpoExtra as IEdmTaskInstance;

            // get the mapped command from the task properties
            _taskSettings = Deserialize(instance.GetValEx("TaskSettings").ToString());

            // create logger 

            try {
                instance.Log("Starting Task");

                // set the status and progress of the task
                int index = 0;
                instance.SetStatus(EdmTaskStatus.EdmTaskStat_Running);
                instance.SetProgressRange(System.IO.Directory.EnumerateFiles(_taskSettings.AgileXmlFolder, "*.xml", SearchOption.TopDirectoryOnly).Count(), index, "Running task...");

                // for each xml file, get the affected items
                // for each affected item, get the converted files
                // move the converted files to the AgileDocumentFolder (zip accordingly)
                // move the xml to the AgileWatchFolder
                foreach (var xmlFile in System.IO.Directory.EnumerateFiles(_taskSettings.AgileXmlFolder, "*.xml", SearchOption.TopDirectoryOnly)) {

                    // if the user is trying to cancel the task just get out
                    if (instance.GetStatus() == EdmTaskStatus.EdmTaskStat_CancelPending) {
                        instance.SetProgressPos(index++, "Cancelled");
                        instance.SetStatus(EdmTaskStatus.EdmTaskStat_DoneCancelled);
                        return;
                    }

                    instance.Log($"Reading XML File: {xmlFile}");
                    instance.SetProgressPos(index++, $"Reading {Path.GetFileName(xmlFile)}");

                    // read the agile xml file
                    var coNumber = XDocument.Load(xmlFile).Root.Element("CO_Number").Value;
                    var status = XDocument.Load(xmlFile).Root.Element("Status").Value;

                    instance.Log($" {coNumber} status is {status}.");

                    var affectedItems = XDocument.Load(xmlFile).Root.Descendants("drawing")
                        .Select(d => new {
                            DrawingNumber = d.Element("Drawing_Number").Value,
                            DrawingName = d.Element("Drawing_Name").Value
                        });

                    instance.Log($"  Affected Items: {affectedItems.Count()}");
                    instance.Log($"  Drawing Number, Drawing Name");
                    foreach(var a in affectedItems) {
                        instance.Log($"  {a.DrawingNumber}, {a.DrawingName}");
                    }

                    var agileStatus = _taskSettings.AgileStatuses.FirstOrDefault(t => status.StartsWith(t.Status, StringComparison.InvariantCultureIgnoreCase));

                    if(agileStatus == null) {
                        throw new Exception($"Invalid Agile status in settings: {status}.");
                    }

                    instance.Log($" Searching for {coNumber}.txt.");

                    IEdmSearch5 search = (IEdmSearch5)vault.CreateUtility(EdmUtility.EdmUtil_Search);
                    search.FileName = coNumber + ".txt";

                    IEdmSearchResult5 searchResult = search.GetFirstResult();
                    if(searchResult == null) {
                        throw new Exception($"Changer order file {coNumber}.txt was not found.");
                    }

                    IEdmFile10 file = (IEdmFile10)vault.GetObject(EdmObjectType.EdmObject_File, searchResult.ID);
                    instance.Log($" Change state on {file.Name} to {agileStatus.StateName}.");
                    file.ChangeState(agileStatus.StateName, searchResult.ParentFolderID, $"Agile change order {coNumber} was {status}.", 0, (int)EdmStateFlags.EdmState_Simple);

                    // move the xml file
                    instance.Log($" Moving {Path.GetFileName(xmlFile)} to {_taskSettings.CompleteAgileXmlFolder}.");
                    File.Move(xmlFile, Path.Combine(_taskSettings.CompleteAgileXmlFolder, Path.GetFileName(xmlFile)));

                }
                instance.SetProgressPos(index, "");
                instance.SetStatus(EdmTaskStatus.EdmTaskStat_DoneOK, 0, "");
            }
            catch (Exception ex) {
                if (instance != null) {
                    instance.SetProgressPos(0, "");
                    instance.SetStatus(EdmTaskStatus.EdmTaskStat_DoneFailed, 0, ex.Message, null, ex.InnerException != null ? ex.InnerException.Message : "");

                    instance.Log($"**** FATAL ERROR:     {ex.Message}");
                    if (ex.InnerException != null) {
                        instance.Log($"****  Inner Exception\t{ex.InnerException.Message}");
                    }
                }
            }
        }

        /// <summary>
        /// This call is made to your add-in when the task definition property dialog box is called. 
        /// The call makes it possible for you to add your own custom pages to the wizard. 
        /// 
        /// The EdmCmd::mpoExtra pointer points to IEdmTaskProperties, the interface of the task definition.
        /// 
        /// Update the IEdmTaskProperties::TaskFlags property to inform the framework about what your add-in is capable of.
        /// </summary>
        /// <param name="poCmd"></param>
        /// <param name="ppoData"></param>
        private void OnTaskSetup(EdmCmd poCmd, EdmCmdData[] ppoData)
        {
            IEdmTaskProperties props = poCmd.mpoExtra as IEdmTaskProperties;

            // this task addin supports transitions
            props.TaskFlags = (int)EdmTaskFlag.EdmTask_SupportsInitExec | (int)EdmTaskFlag.EdmTask_SupportsDetails | (int)EdmTaskFlag.EdmTask_SupportsScheduling;

            // create the custom setup page
            _taskSettings = Deserialize(props.GetValEx("TaskSettings").ToString());

            if (_taskSettings == null) {
                // if the mapped command is null,
                // this is the first time we are setting up the task
                _taskSettings = new TaskSettings();
            }

            TaskSetupPage setupPage = new TaskSetupPage();
            setupPage.Settings = _taskSettings;

            // add the custom setup page to the task setup
            EdmTaskSetupPage[] pages = new EdmTaskSetupPage[] {
                new EdmTaskSetupPage() {
                    mbsPageName = "Settings",
                    mlPageHwnd = setupPage.Handle.ToInt32(),
                    mpoPageImpl = setupPage
                }
            };

            props.SetSetupPages(pages);
        }


        /// <summary>
        /// This call is made to your add-in when the task definition property dialog box is closed. 
        /// 
        /// The EdmCmd::mbsComment string is either "OK" or "Cancel", depending on how the dialog box closed. 
        /// (The string is the same in all localized versions of the program.) 
        /// The call makes it possible for you to save your own properties when OK is clicked. 
        /// You can prevent the dialog box from closing by setting the EdmCmd::mbCancel member to true. 
        /// You can return the name of an add-in page to set focus to in the EdmCmd::mbsComment string if EdmCmd::mbCancel is set to true. 
        /// EdmCmd::mpoExtra points to IEdmTaskProperties, the interface of the task definition.
        /// 
        /// None of the members in the EdmCmdData structure are used.
        /// </summary>
        /// <param name="poCmd"></param>
        /// <param name="ppoData"></param>
        private void OnTaskSetupButton(EdmCmd poCmd, EdmCmdData[] ppoData)
        {
            // we have to serialize the TaskSettings
            IEdmTaskProperties props = (IEdmTaskProperties)poCmd.mpoExtra;

            if (poCmd.mbsComment == "OK") {
                props.SetValEx("TaskSettings", Serialize(_taskSettings));
            }
        }

        #endregion

        #region Private Methods

        private TaskSettings Deserialize(string xml)
        {
            try {
                XmlSerializer serializer = new XmlSerializer(typeof(TaskSettings));
                return (TaskSettings)serializer.Deserialize(new StringReader(xml));
            }
            catch {
                return null;
            }
        }

        private string Serialize(TaskSettings taskSettings)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(TaskSettings));
            StringWriter writer = new StringWriter();
            serializer.Serialize(writer, taskSettings);
            return writer.ToString();
        }

        #endregion

        #region Debugger

        private void AttachDebugger()
        {
            System.Diagnostics.Process process = System.Diagnostics.Process.GetCurrentProcess();
            System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();

            if (!System.Diagnostics.Debugger.IsAttached) {
                if (System.Windows.Forms.MessageBox.Show("Attach Debugger?", "", System.Windows.Forms.MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK) {
                    System.Diagnostics.Debugger.Launch();
                }
            }
        }

        #endregion
    }
}
